/*
 * Time.h
 *
 *  Created on: Mar 14, 2010
 *      Author: As_You_Wish
 */

#ifndef TIME_H_
#define TIME_H_
#include <avr/io.h>
class Time {
public:
	Time();
	virtual ~Time();
	static uint32_t timer1_ms();
	static void sleep_ms(uint32_t time_ms);
};


#endif /* TIME_H_ */

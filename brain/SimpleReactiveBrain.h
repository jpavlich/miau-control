/*
 * SimpleReactiveBrain.h
 *
 *  Created on: May 21, 2010
 *      Author: As_You_Wish
 */

#ifndef SIMPLEREACTIVEBRAIN_H_
#define SIMPLEREACTIVEBRAIN_H_

#include "Brain.h"

class SimpleReactiveBrain: public Brain {
public:
	SimpleReactiveBrain();
	virtual ~SimpleReactiveBrain();
	void update();
};

#endif /* SIMPLEREACTIVEBRAIN_H_ */

#include "Robot.h"
/*
 * Brain.h
 *
 *  Created on: May 21, 2010
 *      Author: As_You_Wish
 */

#ifndef BRAIN_H_
#define BRAIN_H_
class Robot;
class Brain {
protected:
public:
	Robot* robot;
	Brain();
	virtual ~Brain();

	virtual void update() = 0;
};

#endif /* BRAIN_H_ */

/*
 * SimpleReactiveBrain.cpp
 *
 *  Created on: May 21, 2010
 *      Author: As_You_Wish
 */

#include "SimpleReactiveBrain.h"

SimpleReactiveBrain::SimpleReactiveBrain()  {

}

SimpleReactiveBrain::~SimpleReactiveBrain() {
	// TODO Auto-generated destructor stub
}

void SimpleReactiveBrain::update() {

	// Finish standing up when tilted to left or right
	if (robot->action == robot->left_stand_up_action || robot->action == robot->right_stand_up_action) {
		// FIXME isFinished check must be for all elements in the array, not just one
		if (robot->action[0]->isFinished()) {
			robot->resetAction(robot->stand_up_action,false);
		}
	} else if (robot->action == robot->stand_up_action) { // Starts walking after standing up
		if (robot->action[0]->isFinished()) {
			robot->resetAction(robot->walk_action);
		}
	} else if (robot->action[0]->isFinished()) { // Default is to walk after any action has been finished
				robot->resetAction(robot->walk_action);
	}

	if (robot->sensors[SENSOR_LL]->hasBeenOn(400)) { // Robot is tilted to the left side
		robot->sensors[SENSOR_LL]->resetTurnedOnTime();
		robot->setAction(robot->left_stand_up_action, false);
	} else if (robot->sensors[SENSOR_RL]->hasBeenOn(400)) { // Robot is tilted to the right side
		robot->sensors[SENSOR_RL]->resetTurnedOnTime();
		robot->setAction(robot->right_stand_up_action, false);
	} else if (robot->sensors[SENSOR_LM]->hasBeenOn(400)) { // Left mustache is touching something
		robot->sensors[SENSOR_LM]->resetTurnedOnTime();
		if (robot->action == robot->walk_action) {
			robot->resetAction(robot->turn_walk_action,false, false); // Turn right
		}
	} else if (robot->sensors[SENSOR_RM]->hasBeenOn(400)) { // Right mustache is touching something
		robot->sensors[SENSOR_RM]->resetTurnedOnTime();
		if (robot->action == robot->walk_action) {
			robot->resetAction(robot->turn_walk_action,false); // Turn left
		}
//	} else if (robot->sensors[SENSOR_NOSE]->hasBeenOn(400)) { // Robot nose is touching something
//		robot->sensors[SENSOR_NOSE]->resetTurnedOnTime();
//		// FIXME This is not optimal, the system should represent walf forward and walk backward as different actions
//		robot->resetAction(robot->walk_action, true, false); // Walk backwards
	} else if (robot->button->hasBeenOn(400)) {
		robot->button->resetTurnedOnTime();
		if (robot->action == robot->walk_action) {
			robot->resetAction(robot->stop_action);
		} else if (robot->action == robot->stop_action) {
			robot->resetAction(robot->walk_action);
		}
	}
}

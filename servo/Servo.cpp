/*
 * Servo.cpp
 *
 *  Created on: Mar 13, 2010
 *      Author: Jaime Pavlich-Mariscal
 */

#include "Servo.h"
extern "C" {
	#include "SoR_Utils.h"
}
#include "Time.h"

Servo::~Servo() {
	// TODO Auto-generated destructor stub
}

Servo::Servo(char* name, int center, volatile uint8_t *port_letter, int port_number) :
	 center(center),  port_letter(port_letter), port_number(port_number)  {
	this->name = name;

}

void Servo::update() {
	if (state.enabled && (Time::timer1_ms() - last_time_moved > 20)) {
		servo(*port_letter, port_number, abs(center + state.position));
		last_time_moved = Time::timer1_ms();
	}
}


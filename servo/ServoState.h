/*
 * ServoState.h
 *
 *  Created on: Mar 14, 2010
 *      Author: As_You_Wish
 */

#ifndef SERVOSTATE_H_
#define SERVOSTATE_H_

struct ServoState {
	bool enabled;
	int position;
};

#endif /* SERVOSTATE_H_ */

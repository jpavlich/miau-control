/*
 * ServoCommand.h
 *
 *  Created on: Mar 14, 2010
 *      Author: As_You_Wish
 */

#ifndef SERVOCOMMAND_H_
#define SERVOCOMMAND_H_

#include "ServoState.h"



struct ServoCommand {
	ServoCommand(ServoState& state, uint32_t duration) : state(state), duration(duration) {};
	ServoCommand(bool enabled, int position, uint32_t duration) : duration(duration) {
		state.enabled = enabled;
		state.position = position;
	}
	ServoState state;
	uint32_t duration;
};

#endif /* SERVOCOMMAND_H_ */

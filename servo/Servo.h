/*
 * Servo.h
 *
 *  Created on: Mar 13, 2010
 *      Author: Jaime Pavlich-Mariscal
 */

#ifndef SERVO_H_
#define SERVO_H_
#include <avr/io.h>
#include "ServoState.h"
class Servo {


private:

	int center;
	volatile uint8_t* port_letter;
	int port_number;
	uint32_t last_time_moved;


public:
	char* name;
	struct ServoState state;



	Servo(char* name, int center, volatile uint8_t* port_letter, int port_number);
	virtual ~Servo();
	void update();
};

#endif /* SERVO_H_ */

/*
 * ServoCommandSequence.h
 *
 *  Created on: Mar 14, 2010
 *      Author: As_You_Wish
 */

#ifndef SERVOACTION_H_
#define SERVOACTION_H_

#include "Servo.h"
#include "ServoCommand.h"
#include "LinkedList.h"


class ServoAction {
private:
	ListIterator* it;
	uint32_t next_update_time;
	LinkedList* sequence;
public:
	Servo* servo;
	bool cyclic;
	bool forward;


	ServoAction(Servo* servo);
	virtual ~ServoAction();
	void add(bool enabled, int position, uint32_t duration);

	void reset(bool cyclic, bool forward=true);
	void update();

	bool isFinished();


};

#endif /* SERVOACTION_H_ */

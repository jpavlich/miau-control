/*
 * Sensor.h
 *
 *  Created on: Apr 3, 2010
 *      Author: As_You_Wish
 */

#ifndef SENSOR_H_
#define SENSOR_H_
extern "C" {
#include <avr/io.h>
}
class Sensor {
private:
	uint32_t turned_on_since;
	bool was_on_before;
public:
	Sensor();
	virtual ~Sensor();

	virtual bool isOn() = 0;
	virtual bool hasBeenOn(uint32_t ms);
	virtual void resetTurnedOnTime();
	virtual unsigned short readData() = 0;
	virtual void update();
};




#endif /* SENSOR_H_ */

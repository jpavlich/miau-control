/*
 * Sensor10bit.cpp
 *
 *  Created on: Apr 3, 2010
 *      Author: As_You_Wish
 */

#include "Sensor10bit.h"
extern "C" {
#include "a2d.h"
}

Sensor10bit::Sensor10bit(unsigned char pin) : Sensor(), pin (pin){

}

Sensor10bit::~Sensor10bit() {

}

inline unsigned short Sensor10bit::readData() {
	return a2dConvert10bit(pin);
}

bool Sensor10bit::isOn()
{
	return readData() > 1022;
}



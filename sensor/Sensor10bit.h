/*
 * Sensor10bit.h
 *
 *  Created on: Apr 3, 2010
 *      Author: As_You_Wish
 */

#ifndef SENSOR10BIT_H_
#define SENSOR10BIT_H_
#include "Sensor.h"

class Sensor10bit : public Sensor {
private:
	unsigned char pin;
public:
	Sensor10bit(unsigned char pin);
	virtual ~Sensor10bit();
	unsigned short readData();
	bool isOn();
};

#endif /* SENSOR10BIT_H_ */

/*
 * BinarySensor.h
 *
 *  Created on: Apr 2, 2010
 *      Author: As_You_Wish
 */

#ifndef BINARYSENSOR_H_
#define BINARYSENSOR_H_
#include <avr/io.h>
#include "Sensor.h"
class Sensor1bit : public Sensor {
private:
	volatile uint8_t* port_letter;
	int port_number;

public:
	Sensor1bit(volatile uint8_t* port_letter, int port_number);
	bool isOn();
	unsigned short readData();
	virtual ~Sensor1bit();
};

#endif /* BINARYSENSOR_H_ */

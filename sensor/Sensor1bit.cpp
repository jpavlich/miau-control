/*
 * BinarySensor.cpp
 *
 *  Created on: Apr 2, 2010
 *      Author: As_You_Wish
 */

#include "Sensor1bit.h"
#include "Time.h"
Sensor1bit::Sensor1bit(volatile uint8_t* port_letter, int port_number) :
	Sensor(), port_letter(port_letter), port_number(port_number) {

}

bool Sensor1bit::isOn() {
	return bit_is_clear((*port_letter), port_number);
}

unsigned short Sensor1bit::readData() {
	return bit_is_clear((*port_letter), port_number);
}

Sensor1bit::~Sensor1bit() {

}

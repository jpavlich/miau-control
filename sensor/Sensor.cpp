/*
 * Sensor.cpp
 *
 *  Created on: Apr 3, 2010
 *      Author: As_You_Wish
 */

#include "Sensor.h"
#include "Time.h"

Sensor::Sensor() {
	turned_on_since = 0;
	was_on_before = false;
}

Sensor::~Sensor() {
	// TODO Auto-generated destructor stub
}

void Sensor::update() {
	bool is_on_now = isOn();
	if (!was_on_before && is_on_now) {
		turned_on_since = Time::timer1_ms();
	}
	was_on_before = is_on_now;
}



bool Sensor::hasBeenOn(uint32_t ms) {
	if (was_on_before && isOn()) {
		return (Time::timer1_ms() - turned_on_since) > ms;
	} else {
		return false;
	}
}


void Sensor::resetTurnedOnTime()
{
		turned_on_since = Time::timer1_ms();
}

/*
 * ListIterator.cpp
 *
 *  Created on: Mar 13, 2010
 *      Author: As_You_Wish
 */

#include "ListIterator.h"
#include "global_defs.h"
ListIterator::ListIterator(bool cyclic) {
	this->cyclic = cyclic;
	current = NULL;
	list = NULL;
	beforeStart = false;
	afterEnd = false;

}

ListIterator::~ListIterator() {
	// TODO Auto-generated destructor stub
}

void ListIterator::reset()
{
	this->current = list->first;
	beforeStart = false;
	afterEnd = false;


}

void* ListIterator::next()
{
	if (current == NULL) {
		if (cyclic) {
			current = list->first;
		} else {
			current = list->last;
			afterEnd = true;
		}
		if (current == NULL) {
			return NULL;
		}
	}
	void* data = current->data;
	current = current->next;
	return data;
}

void* ListIterator::previous() {
	if (current == NULL) {
		if (cyclic) {
			current = list->last;
		} else {
			current = list->first;
			beforeStart = true;
		}
		if (current == NULL) {
			return NULL;
		}
	}
	void* data = current->data;
	current = current->previous;
	return data;
}

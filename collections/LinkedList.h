/*
 * LinkedList.h
 *
 *  Created on: Mar 13, 2010
 *      Author: As_You_Wish
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

#include "ListNode.h"
#include "ListIterator.h"

class ListNode;
class ListIterator;
class LinkedList {
	friend class ListIterator;
private:
	ListNode* first;
	ListNode* last;

public:
	LinkedList();
	virtual ~LinkedList();
	void add(void* elem);
	ListIterator* iterator(bool cyclic = true);

};

#endif /* LINKEDLIST_H_ */


/*
 * ListNode.h
 *
 *  Created on: Mar 13, 2010
 *      Author: Jaime Pavlich-Mariscal
 */

#ifndef LISTNODE_H_
#define LISTNODE_H_
#include "LinkedList.h"
#include "ListIterator.h"
class LinkedList;
class ListNode {
	friend class LinkedList;
	friend class ListIterator;
private:
	LinkedList* list;
	ListNode* previous;
	ListNode* next;
	void* data;
public:
	ListNode();
	virtual ~ListNode();
};

#endif /* LISTNODE_H_ */

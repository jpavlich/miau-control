/*
 * LinkedList.cpp
 *
 *  Created on: Mar 13, 2010
 *      Author: As_You_Wish
 */

#include "LinkedList.h"
#include "global_defs.h"

LinkedList::LinkedList() {
	first = NULL;
	last = NULL;

}

LinkedList::~LinkedList() {
	ListNode* n = first;
	ListNode* next;
	while (n!= NULL) {
		next = n->next;
		delete(n);
		n = next;
	}
}

void LinkedList::add(void* elem) {
	ListNode* n = new ListNode();
	n->list = this;
	if (this->first == NULL) {
		this->first = n;
		this->last = n;
		n->previous = NULL;
		n->next = NULL;
	} else {
		this->last->next = n;
		n->previous = this->last;
		n->next = NULL;
		this->last = n;
	}
	n->data = elem;
}


ListIterator *LinkedList::iterator(bool cyclic) {
	ListIterator* it = new ListIterator(cyclic);
	it->list=this;
	it->reset();
	return it;
}


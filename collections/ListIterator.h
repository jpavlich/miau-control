/*
 * ListIterator.h
 *
 *  Created on: Mar 13, 2010
 *      Author: As_You_Wish
 */

#ifndef LISTITERATOR_H_
#define LISTITERATOR_H_
#include "ListNode.h"
#include "LinkedList.h"
class ListNode;
class LinkedList;
class ListIterator {
	friend class LinkedList;
private:
	ListNode* current;
	LinkedList* list;
public:
	ListIterator(bool cyclic = true);
	virtual ~ListIterator();
	void reset();
	void* next();
	void* previous();
	bool cyclic;
	bool beforeStart;
	bool afterEnd;
};

#endif /* LISTITERATOR_H_ */

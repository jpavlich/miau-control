/****************************************************************************
*
*   Copyright (c) 2008 www.societyofrobots.com
*   (please link back if you use this code!)
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License version 2 as
*   published by the Free Software Foundation.
*
*   Alternatively, this software may be distributed under the terms of BSD
*   license.
*
****************************************************************************/
#ifndef SOR_UTILS_H

#define SOR_UTILS_H
//AVR includes
#include <avr/io.h>		    // include I/O definitions (port names, pin names, etc)
#include <avr/interrupt.h>	// include interrupt support
#include <stdio.h>			// stuff
#include <stdlib.h>			// stuff
#include <math.h>			// stuff
//#include "libm.a"			// required with math.h
#include <string.h>			// allow strings to be used
//#include <avr/eeprom.h>		// adds EEPROM functionality

//AVRlib includes
#include "global.h"		// include global settings
#include "uart4.h"		// include uart function library, includes buffer.h
#include "rprintf.h"	// include printf function library
#include "timer640.h"	// include timer function library (timing, PWM, etc)
//#include "pwm.h"		// for PWM stuff
#include "a2d.h"		// include A/D converter function library
//#include "i2c.h"		// include i2c support
//#include "spi.h"		// include spi support

//define port functions; example: PORT_ON( PORTD, 6);
#define PORT_ON( port_letter, number )			port_letter |= (1<<number)
#define PORT_OFF( port_letter, number )			port_letter &= ~(1<<number)
//#define PORT_ALL_ON( port_letter, number )		port_letter |= (number)
//#define PORT_ALL_OFF( port_letter, number )		port_letter &= ~(number)
#define FLIP_PORT( port_letter, number )		port_letter ^= (1<<number)
#define PORT_IS_ON( port_letter, number )		( port_letter & (1<<number) )
#define PORT_IS_OFF( port_letter, number )		!( port_letter & (1<<number) )

//define the servo function macro
#define servo(port,number,position)   (PORT_ON(port,number), delay_cycles(position), PORT_OFF(port,number))


//************CONFIGURE PORTS************
//configure ports for input or output - specific to ATmega2560
void configure_ports(void);
//***************************************


//************DELAY FUNCTIONS************
//wait for X amount of cycles (234 cycles is about 1.003 milliseconds)//incorrect
//to calculate: 234/1.003*(time in milliseconds) = number of cycles
void delay_cycles(unsigned long int cycles);
//***************************************


//***************STATUS LED**************
void LED_off(void);
void LED_on(void);
//***************************************


//*****************BUTTON****************
int button_pressed(void);
//***************************************




signed int cos_SoR(long signed int degrees);

signed int sin_SoR(long signed int degrees);

signed int tan_SoR(long signed int degrees);
#endif

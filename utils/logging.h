#ifndef LOGGING_H
#define LOGGING_H
extern "C" {
#include "rprintf.h"
}
#define MIAU_LOG3


#ifdef MIAU_LOG1
	#define ERROR(...)
	#define LOG(...)   rprintf(__VA_ARGS__)
	#define DEBUG(...)
#else

	#ifdef MIAU_LOG2
		#define ERROR(...)   rprintf(__VA_ARGS__)
		#define LOG(...)   rprintf(__VA_ARGS__)
		#define DEBUG(...)
	#else

		#ifdef MIAU_LOG3
			#define ERROR(...)   rprintf(__VA_ARGS__)
			#define LOG(...)   rprintf(__VA_ARGS__)
			#define DEBUG(...)   rprintf(__VA_ARGS__)
		#else
			#define DEBUG(...)
			#define ERROR(...)
			#define LOG(...)

		#endif
	#endif
#endif


#endif

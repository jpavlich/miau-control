/*
 * MIAU.h
 *
 *  Created on: Mar 13, 2010
 *      Author: As_You_Wish
 */

#ifndef MIAU_H_
#define MIAU_H_

#include "Robot.h"


class MIAU {
private:
	Robot* robot;

public:
	MIAU();
	virtual ~MIAU();
	void init();
	void begin();

};




#endif /* MIAU_H_ */

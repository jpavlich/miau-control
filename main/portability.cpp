/* Code obtained from
 * http://www.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&p=198679
 * and
 * http://www.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&p=458391
 * I assume that this code is open source. If you know it's not
 * please let me know to remove it
 */
#include <stdlib.h>

void* operator new(size_t size) {
	return malloc(size);
}
void operator delete(void* ptr) {
	free(ptr);
}
void * operator new[](size_t size) {
	return malloc(size);
}

void operator delete[](void * ptr) {
	free(ptr);
}
__extension__ typedef int __guard __attribute__((mode (__DI__)));

extern "C" int __cxa_guard_acquire(__guard *g);

extern "C" void __cxa_guard_release(__guard *g);
extern "C" void __cxa_guard_abort(__guard *);

int __cxa_guard_acquire(__guard *g) {
	return !*(char *) (g);
}

void __cxa_guard_release(__guard *g) {
	*(char *) g = 1;
}

void __cxa_guard_abort(__guard *) {
}


extern "C" void __cxa_pure_virtual();
extern "C" void _pure_virtual(void);
extern "C" void __pure_virtual(void);
extern "C" int atexit(void(*func)(void));
extern "C" int __cxa_atexit();
void __cxa_pure_virtual() {
}

void _pure_virtual() {
}

void __pure_virtual() {
}

int atexit(void(*func)(void)) {
	return -1;
}
int __cxa_atexit() {
	return -1;
}

/*
 * Robot.h
 *
 *  Created on: Mar 14, 2010
 *      Author: As_You_Wish
 */

#ifndef ROBOT_H_
#define ROBOT_H_

#define NUM_SERVOS 11
#define NUM_SENSORS 17

#define SERVO_UFLL 0
#define SERVO_UFRL 1
#define SERVO_UBLL 2
#define SERVO_UBRL 3
#define SERVO_LFLL 4
#define SERVO_LFRL 5
#define SERVO_LBLL 6
#define SERVO_LBRL 7
#define SERVO_LN 8
#define SERVO_UN 9
#define SERVO_T 10

#define SENSOR_BUTTON 0
#define SENSOR_LL 1
#define SENSOR_RL 2
#define SENSOR_NOSE 3
#define SENSOR_LM 4
#define SENSOR_RM 5

#include "Servo.h"
#include "ServoAction.h"
#include "Sensor.h"
#include "Sensor1bit.h"
#include "../brain/Brain.h"
#include "../brain/SimpleReactiveBrain.h"
class Robot {
private:
	Servo** servos;

	Brain* brain;

    void initServos();
    void initSensors();
    void initStopAction();
    void initWalkActions();
    void initStandUpActions();
    void walk_pos0(ServoAction** a, int upperLeg, int lowerLeg, uint32_t duration);
    void walk_pos1(ServoAction** a, int upperLeg, int lowerLeg, uint32_t duration);
    void walk_pos2(ServoAction** a, int upperLeg, int lowerLeg, uint32_t duration);
    void walk_pos3(ServoAction** a, int upperLeg, int lowerLeg, uint32_t duration);

public:
	Robot(Brain* brain);
	virtual ~Robot();
	void update();
	void setAction(ServoAction** action, bool cyclic=true, bool forward=true);
	void resetAction(ServoAction** action, bool cyclic=true, bool forward=true);
	ServoAction** action;
	ServoAction** stop_action;
	ServoAction** stand_up_action;
	ServoAction** walk_action;
	ServoAction** turn_walk_action;
	ServoAction** left_stand_up_action;
	ServoAction** right_stand_up_action;

	Sensor** sensors;
	Sensor1bit* button;
};

#endif /* ROBOT_H_ */
